include Math
require 'csv'

class FunctionSolver

	def initialize(x, n)
		@x = x
		@n = n
		@formula = "f(x) = -sin(x^e) * ((n-x)!)^3"
		@result = "x = #{@x}; n = #{@n}; f(x) = #{solve}"
	end

	def solve
		- sin(@x**::E) * function(@n, @x)**3
	end

	def display
		puts "#{@formula} \n #{@result}"
	end

	def save_txt(filename = './solution.txt')
		File.write(filename, "#{@formula} \n #{@result}") 
	end

	def save_csv(filename = './solution.csv')
		CSV.open(filename, 'w') do |csv_object|
			csv_object << ["x", "n", @formula]
			csv_object << [@x, @n, solve]
		end
	end

	private

	def function(n, x = 0)
		if n - x < 2
			1
		else
			(n - x) * function(n - x - 1)
		end
	end

end