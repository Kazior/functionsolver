require './functionsolver.rb'

class UserInterface

	def run 
		choice = menu1 while(choice != '0')
	end

	#private

	def menu1 
		show_menu
		
		choice = STDIN.gets.chop
			
		if choice == '1'
			loop do
				print "\n Input x,n: "
				inp = STDIN.gets.chop
				break if create_F_instance(inp)
			end
		end

		choice
	end

	def create_F_instance(input = '')
		return unless validate_input(input)

		x = input.split(',')[0].to_f
		n = input.split(',')[1].to_i

		solution = FunctionSolver.new(x,n)
		
		loop do
			choice2 = menu2(solution)
			break if choice2 == '0'
		end 

		true
	end

	def menu2(solution)
		show_menu(2)

 		choice = STDIN.gets.chop!
    	
   	case choice
   	when "1"
   		puts "\n\n #{solution.display}"
   	when "2"
   		solution.save_txt
   		puts "solution saved to file: solution.txt"
   	when "3"
   		solution.save_csv
   		puts "solution saved to file: solution.csv"
   	end
		
		choice
	end

	def show_menu(menu = 1)
		case menu
		when 1
			puts "\n0 - exit"
			puts "1 - create FunctionSolver instance"
			print ">"
		when 2
			puts "\n0 - exit"
  		puts "1 - print solution on standard output"
  		puts "2 - print into txt file"
    	puts "3 - print into CSV"
			print ">"
		end
	end

	def validate_input(input)		
		fail if input.empty?

		x = input.split(',')[0]
		n = input.split(',')[1]
		
		return unless is_number?(x) && is_number?(n) && n_integer?(n) && x_range(x.to_f) && n_range(n.to_i)

		true
	rescue
		puts "error: empty input"
	end

  def is_number?(var)
    s = var.to_s

		return true if s.match(/\A\d+\Z/) || s.match(/\A\d+\.\d+\Z/) # super sprawa :)

    fail

    # fail unless s.match(/\A\d+\Z/) || s.match(/\A\d+\.\d+\Z/)

    # true

  rescue
    puts "error: wrong input format, two numbers separated by /,/ expected"
  end

	def n_integer?(n)
		fail if n.to_s.match(/\./)
		true
	rescue
		puts "error: n is not integer number"
	end

	def x_range(x)
		fail if x.abs > 999
		true		
	rescue
		puts "error: x is out of range, <-999..999> expected "
	end

	def n_range(n)
		fail if n > 99 || n < 0
		true
	rescue
		puts "errox: n is out of range, <0..99> expected"
	end
end

def run
	ui = UserInterface.new
	ui.run
end
